/*
Given an array of integers arr, write a function absSort(arr),
that sorts the array according to the absolute values of the numbers in arr.
If two numbers have the same absolute value, sort them according to sign,
where the negative numbers come before the positive numbers.

Examples:

input:  arr = [2, -7, -2, -2, 0]
output: [0, -2, -2, 2, -7]
*/


leafNodes = [];

class Node{
    constructor(val){
        this.left = null;
        this.right = null;
        this.val = val;
    }

    insert(val){
        if(Math.abs(val) == Math.abs(this.val)){
            if(val < this.val){
                if(this.left == null) this.left = new Node(val);
                else this.left.insert(val);
            } else if (val > this.val){
                if(this.right == null) this.right = new Node(val);
                else this.right.insert(val);
            } else {
                if(this.left == null) this.left = new Node(val);
                else this.left.insert(val);
            }
        }
        else if(Math.abs(val) < Math.abs(this.val)){
            if(!this.left) this.left = new Node(val);
            else this.left.insert(val);
            
        } else if (Math.abs(val) > Math.abs(this.val)){
            if(!this.right) this.right = new Node(val);
            else this.right.insert(val);
        } else {
            this.val = val;
        }
    }

    

    buildLeafNodesList(node){
        
        // if(node.left) node.left.buildLeafNodesList(node.left);
        if(node.left)
            node.left.buildLeafNodesList(node.left); 
        // console.log("node: ", node.val);
        leafNodes.push(node.val);
        // console.log("leafNodes: ", leafNodes);
        if(node.right)
            node.right.buildLeafNodesList(node.right);
    }

    //preorder
    dfs(node){
        if(node){
            console.log(node.val);
            this.dfs(node.left);
            this.dfs(node.right);
        }
    }
}



function absSort(arr) {
    let root;
    for(var i = 0; i < arr.length; i++){
        if(i == 0) root = new Node(arr[i]);
        else root.insert(arr[i]);
    }

    root.buildLeafNodesList(root);
    console.log(leafNodes);
    // console.log(root);
    // root.dfs(root);
}

// absSort([2, -7, 1, -8, 7, 5, 34, 13 ,321]);
absSort([2, -7, -2, -2, 0]);
// absSort([0])